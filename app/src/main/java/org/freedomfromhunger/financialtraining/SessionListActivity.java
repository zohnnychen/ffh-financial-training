package org.freedomfromhunger.financialtraining;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


public class SessionListActivity extends FragmentActivity {

    private static final String TAG = "SessionListAct";
    private Module mModule;
    private SessionAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_list);
        mModule = Module.loadDefaultLanguageAndModule(this);
        mAdapter = new SessionAdapter(this, mModule.sessions);
        ListView list = (ListView) findViewById(R.id.list);
        list.setAdapter(mAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Module.Session session = (Module.Session) adapterView.getItemAtPosition(i);
                if (session == null || session.name == null) {
                    Log.w(TAG, "Couldn't resolve session");
                    return;
                }
                Intent intent = new Intent(SessionListActivity.this, SessionActivity.class);
                intent.putExtra(SessionActivity.INTENTARG_SESSIONNAME, session.name);
                startActivity(intent);
            }
        });

    }

    private class SessionAdapter extends ArrayAdapter<Module.Session> {

        public SessionAdapter(Context context, Module.Session[] sessions) {
            super(context, 0, sessions);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View root = LayoutInflater.from(SessionListActivity.this).inflate(R.layout.listitem_session, parent, false);
            Module.Session session = getItem(position);
            ImageView image = (ImageView) root.findViewById(R.id.image);
             Uri uri = Module.get(session.listItem);
            Picasso.with(SessionListActivity.this).load(uri).into(image);
            return root;
        }
    }
}
