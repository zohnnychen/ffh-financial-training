package org.freedomfromhunger.financialtraining;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

/**
 * Created by jchen on 11/9/15.
 */
public class ViewVideoActivity extends Activity implements MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener {

    private static final String TAG = "ViewVideoAct";
    private VideoView mVideoView;

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.activity_video);

        // Init buttons
        findViewById(R.id.button_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        mVideoView = (VideoView)findViewById(R.id.video);
        mVideoView.setOnCompletionListener(this);
        mVideoView.setOnPreparedListener(this);
        MediaController mediaController = new
                MediaController(this);
        mediaController.setAnchorView(mVideoView);
        mVideoView.setMediaController(mediaController);

        playVideo();
    }

    private void playVideo() {
        if (getIntent().getData() == null) {
            Log.w(TAG, "No video URI specified");
            finish();
            return;
        }
        mVideoView.setVideoURI(getIntent().getData());
        mVideoView.start();
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        playVideo();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mVideoView != null && mVideoView.isPlaying()) {
            mVideoView.stopPlayback();
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        finish();
    }


    @Override
    public void onPrepared(MediaPlayer mp) {
//        mp.setLooping(true);
    }
}
