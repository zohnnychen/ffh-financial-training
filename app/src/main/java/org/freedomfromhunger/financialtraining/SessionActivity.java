package org.freedomfromhunger.financialtraining;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;

import java.io.IOException;

public class SessionActivity extends FragmentActivity {

    private static final String TAG = "SessionAct";
    private static final int MSG_PAUSE = 100;
    public static final String INTENTARG_SESSIONNAME = "INTENTARG_SESSIONNAME";

    private MediaPlayer mPlayer;
    private Handler mHandler;
    private Module.Session mSession;
    private StepAdapter mAdapter;
    private ProgressBar mProgress;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mProgress = (ProgressBar)findViewById(R.id.progressBar);

        mHandler = new Handler(new Handler.Callback() {
            // Handle the "pause" message
            @Override
            public boolean handleMessage(Message message) {
                if (message.what != MSG_PAUSE) {
                    return false;
                }
                if (mPlayer != null) {
                    try {
                        if (mPlayer.isPlaying()) {
                            Log.d(TAG, "Pausing because step over : " + message.arg1);
                            mPlayer.pause();
                        }
                    } catch (IllegalStateException e) {
                        Log.w(TAG, "Player error while pausing", e);
                    }
                }
                return true;
            }
        });

        try {
            mSession = getSession(getIntent());
        } catch (Exception e) {
            Log.w(TAG,"Unable to find session");
            finish();
            return;
        }

        mPlayer = new MediaPlayer();

        AssetFileDescriptor sessionTitle = Module.descriptor(mSession.audio, this);
        AsyncTask.execute(new PlayAudio(sessionTitle));

        // Init buttons
        findViewById(R.id.button_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        
        // Init views
        ImageView header = (ImageView)findViewById(R.id.header);
        Picasso.with(this).load(Module.get(mSession.listItem)).into(header);
        mAdapter = new StepAdapter(this, mSession.steps);
        ListView list = (ListView)findViewById(R.id.list);
        list.setAdapter(mAdapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Module.Step step = (Module.Step) adapterView.getItemAtPosition(position);
                Log.d(TAG,"At step, mediaAudio: " + step.mediaAudio + ", " + mSession.video);
                if (!step.mediaAudio) {
                    // This is a video step
                    startPlayVideoActivity();
                    return;
                }
                String path = step.audio;
                if (path == null) {
                    Log.w(TAG, "No path found for step " + step.img);
                    return;
                }
                AssetFileDescriptor des = Module.descriptor(path, SessionActivity.this);
                if (des == null ) {
                    Log.w(TAG,"Unable to determine descriptor for step " + path);
                    return;
                }
                AsyncTask.execute(new PlayAudio(des));


            }
        });
        
    }

    private void startPlayVideoActivity() {
        // Get raw identifier
        if (TextUtils.isEmpty(mSession.video)) {
            Log.w(TAG,"No video identified for session. Aborting playback");
            return;
        }
        Intent videoPlaybackActivity = new Intent(this, ViewVideoActivity.class);
        videoPlaybackActivity.setData(Module.getRawVideo(this, mSession.video));
        startActivity(videoPlaybackActivity);
    }


    private class StepAdapter extends ArrayAdapter<Module.Step> {

        public StepAdapter(Context context, Module.Step[] steps) {
            super(context, 0, steps);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View root = LayoutInflater.from(SessionActivity.this).inflate(R.layout.listitem_session, parent, false);
            Module.Step step = getItem(position);
            ImageView image = (ImageView) root.findViewById(R.id.image);
            Uri uri = Module.get(step.img);
            Picasso.with(SessionActivity.this).load(uri).into(image);
            return root;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopProgress();
        if (mPlayer != null) {
            try {
                if (mPlayer.isPlaying()) {
                    mPlayer.pause();
                }
            } catch (Exception e) {
                Log.w(TAG, "Unable to release player", e);
            }
        }
    }

    private class PlayAudio implements Runnable {

        private AssetFileDescriptor mPath;

        public PlayAudio(AssetFileDescriptor path) {
            mPath = path;
        }

        @Override
        public void run() {
            if (mPlayer == null) {
                throw new IllegalStateException("Player not initialized");
            }


            // Pause any ongoing track
            try {
                if (mPlayer.isPlaying()) {
                    mPlayer.pause();
                }
            } catch (Exception e) {
                Log.w(TAG, "Unable to release player", e);
            }
            Log.d(TAG, "Playing file " + mPath);

            // If MediaPlayer already used before and in the wrong state
            mPlayer.reset();

            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

            try {
                Log.d(TAG,"Playing audio: " + mPath);
                mPlayer.setDataSource(mPath.getFileDescriptor(), mPath.getStartOffset(), mPath.getLength());
            } catch (IOException e) {
                Log.w(TAG, "Unable to open player", e);
            }
            mPlayer.prepareAsync();

            mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                    Log.w(TAG, "Resetting. Got Media Player error code " + i + ", " + i1);
                    try {
                        mediaPlayer.reset();
                    } catch (Exception e) {
                        Log.w(TAG, "Unable to reset", e);
                    }
                    stopProgress();
                    return false;
                }
            });


            mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(final MediaPlayer mediaPlayer) {
                    Log.d(TAG, "Player prepared ");

                    try {
                        if (mediaPlayer.isPlaying()) {
                            Log.d(TAG, "Interupting current playback");
                            mediaPlayer.pause();
                        }
                    } catch (IllegalStateException e) {
                        Log.w(TAG, "Player error", e);
                    }

                    try {
                        // Eliminate any pending pause requests
                        mHandler.removeMessages(MSG_PAUSE);

                        // Start progress bar
                        if (mProgress != null) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mProgress.setVisibility(View.VISIBLE);
                                }
                            });
                        }

                        // Start playing
                        mediaPlayer.start();


                    } catch (IllegalStateException e) {
                        Log.w(TAG, "Player error", e);
                    }
                }
            });
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    stopProgress();

                }
            });
            // Init handler that will pause playback
        }
    }

    private void stopProgress() {
        // Start progress bar
        if (mProgress != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProgress.setVisibility(View.INVISIBLE);
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPlayer != null) {
            mPlayer.release();
        }
    }

    private Module.Session getSession(Intent intent) {
        String name = intent.getStringExtra(INTENTARG_SESSIONNAME);

        if (TextUtils.isEmpty(name)) {
            throw new IllegalArgumentException("No session specified");
        }

        Module module = Module.loadDefaultLanguageAndModule(this);
        for (Module.Session session : module.sessions){
            if (session != null && session.name != null && session.name.equals(name)) {
                return session;
            }
        }
        throw new IllegalArgumentException("Didn't find any session matching name " + name);
    }


}
