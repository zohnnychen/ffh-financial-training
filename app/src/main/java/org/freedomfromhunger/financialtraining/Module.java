package org.freedomfromhunger.financialtraining;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 * All data for the Financial Training Module (Airtel Mobile Money)
 * <p/>
 * Created by jchen on 10/23/15.
 */
public class Module {

    /**
     * @param mediaPath relative mediapath under whatever parent folder media lives in.
     * @return correct absolute Uri for that media path
     */
    public static Uri get(String mediaPath) {
        return Uri.parse("file:///android_asset/" + mediaPath);
    }
    public static AssetFileDescriptor descriptor(String mediaPath, Context context) {
        try {
            return context.getAssets().openFd(mediaPath);
        } catch (IOException e) {
            return null;
        }
    }

    public static Uri getRawVideo(Context context, String relativeVideoName) {
        int res=context.getResources().getIdentifier(relativeVideoName, "raw", context.getPackageName());
        if (res == 0) {
            throw new IllegalArgumentException("Unable to resolve identifier for resource " + relativeVideoName);
        }
        return Uri.parse("android.resource://" + context.getPackageName() + "/" + res);
    }

    private static final String TAG = "Module";
    public Session[] sessions;

    public static class Session {
        public String name;
        public String video;
        public String audio;
        public Step[] steps;
        public String listItem;
     }

    public static class Step {
        public boolean mediaAudio;
        public String audio;
        public String img;

    }

    /**
     * Loads the default module (module1 in this case) in the default language that came in this APK
     * @param context
     * @return
     */
    public static Module loadDefaultLanguageAndModule(Context context) {
        return loadFromJson(R.raw.module1, context);
    }

    public static Module loadFromJson(int rawRscId, Context context) {

        InputStreamReader reader = null;
        try {
            // IOException if asset doesn't exist
            reader = new InputStreamReader(context.getResources().openRawResource(rawRscId));
            Gson gson = new Gson();
            return gson.fromJson(reader, Module.class);
        } catch (Exception e) {
            Log.w(TAG, "Unable to load JSON", e);
            return null;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.w(TAG, "Unable to clean up reader", e);
                }
            }
        }

    }
}