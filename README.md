# FfH Financial Training 
October 2016, johnny@chenhome.org


## Intro 
This is a training app for community agents that are in the field.  Through this app, a community agent can be assisted in teaching the curriculum for Financial Training.

The main hypothesis we're testing is whether this user interface results in people learning the material.  And, critically, whether this is any better than live teaching (and cheaper). Will this improve the community agent's ability to teach. The goal is to scale.  Will a mobile app allow that?

We've tested versions of this app twice in the field in Burkina Faso, both in 2016.  These tests have validated that this app's user interface approach is workable.  We've validated that video (and audio) are effective means of communicating.  We've also validated that the community agent's can effectively understand and use this application.

## Version History
### Version 1.4.x
- Includes separate Mossi and French APK's (see "How to Build" for instructions)
- Possible to add more languages and build new language-specific APK's easily
- Refined interface so that scrolling is not as necessary
- Updated photos for Sessions and Steps to be latest images from Edouine
- Screen remains on when audio is playing
- Video pause/rewind/fast-forward controls added (when user taps on screen)
- (Not Added) Does not play a sound notification before each audio clip.

#### Download the app
* Download the [Mossi-only App](https://tsfr.io/SRcMGR)
* Download the [French-only App](https://tsfr.io/qKt0Mr)

### Version 1.3.x
- *Tested Summer 2016 in Burkina Faso*
- This app currently supports *French* and *Mossi*.  
- It allows community agents to teach the "Going Mobile" module, which consists of 7 sessions. 
- In this version, the app is built with all language video/audio files included. This isn't so efficient and understandable to the users.  
- For a future version, we're moving to allowing the engineers to build language-specific Android APK variants.

#### Download the app
* [v1.3 Version of App](https://tsfr.io/2yna2d)

## How this app supports multiple languages
The Financial Training app uses Android's [build variant](https://developer.android.com/studio/build/build-variants.html) feature to support creating different APK's for each language it supports.

There's a separate APK for Mossi vs French. This ensures that the APK's can be as small as possible.

Each APK also has a different applicationId (e.g org.freedomfromhunger.financialtraining.mossi) for each language's APK.  This means you can deploy the Mossi APK in a different Google Play Store listing from the French APK.  

### How does the source code support this?
In the source code, there's a manifest file that outlines where each of the video and audio files live for each build variant.

For example, the Mossi audio files live in `app/src/mossi/assets` and `app/src/mossi/res/raw`.

This manifest file specific to language also lives in the language-specific source folder.  For example, the mossi manifest file lives in `app/src/mossi/res/raw/module1_mossi.json`. 


For each language that this source-base supports, there's a language folder. So far we have:

* `app/src/mossi`
* `app/src/french`
* You would add something like `app/src/english`

The important thing about the manifest is that it tells the app where every Video and Audio file lives for every session in a module.  Note that the application is dynamic and based on the data, meaning that you can have as many Sessions as you want in a particular module.  

However, you can only have 5 steps (as that's the number of illustrations we currently have).   As you can see in source, here's how the manifest file links to each audio/video file:

~~~
{
  "title":"Going Mobile in Moore/Mossi",
  "sessions": [
    {
      "name":"session1",
      "listItem":"module1/session1_lineitem.png",
      "audio":"module1/mossi_session1/session1_title.mp3",
      "video":"mossi_session1",
      "steps":[
        {
          "mediaAudio":"true",
          "audio":"module1/mossi_session1/s1step1.mp3",
          "img":"module1/step1.png"
        },
~~~

### Adding new languages
To add a new language, do the following steps.  In these instructions, assume that we're adding the language, "english".

1. Create all the appropriate video and audio files as outlined in the manifest, `app/src/main/res/module1_mossi.json`.
1. Add a new productFlavor in the `ffh-financial-training/build.gradle` file.  Make sure to match the `versionName` value to the current version.  
1. Also note that the `applicationId` contains the language as a suffix.
~~~
   productFlavors {
        english {
            applicationId "org.freedomfromhunger.financialtraining.english"
            versionName "1.4.0-english"
        }
~~~
1. Create the appropriate folders for the audio/video files to live.  This is required by Android's build variant feature.  
~~~
> mkdir app/src/english
> mkdir app/src/english/assets
> mkdir app/src/english/res/raw

// To view where Android build system thinks your source is
> ./gradlew sourceSets
// This will confirm that you created the folders in the right place
~~~

1. Create a new manifest file `app/src/main/res/module1_english.json` and put it in the `app/src/main/res` folder.
1. Make sure the new manifest file appropriately references the new audio/video files that you've created and put in `app/src/english/..`
1. Create a `strings.xml` file for the build variant at `app/src/english/res/values/strings.xml`
1. Update the name of the application in `app/src/english/res/values/strings.xml`

## Development Environment
### Setting up your dev environment 
Install the following tools on your computer.  This is what's in my environment, a Mac OS 10.1:

- [Android Studio 2.2](https://developer.android.com/studio)
- Git 2.8.4, using XCode

Make sure the following components in the Android SDK are installed (or the latest version):

- This is done using the Android Studio, "SDK Manager" feature
- In the menu, `Tools -> Android -> SDK Manager`
- Android 5.1 (Level 22)
- Android SDK Tools 25.2.2
- Android Support Library 38.x
- Google Repository 36

Make sure to update the `sdk.dir` variable
- In `ffh-financial-training/local.properties`, change the `sdk.dir` variable to match your local environment.

"Install" Gradle
- Install gradle by simply running the gradlew wrapper one time from the command line. This will cause the script to download the necessary Gradle components and install them.
~~~
> cd ffh-financial-training
> ./gradlew
~~~

### Dependencies
Gradle Dependency
- gradle 2.2.0, as noted in the `ffh-financial-training/build.gradle` file

App Dependencies
- This is noted in the `app/build.gradle` file
~~~~     
    compile fileTree(dir: 'libs', include: ['*.jar'])
    compile 'com.android.support:appcompat-v7:22.2.0'
    compile 'com.android.support:support-v4:22.2.0'
    compile 'com.google.code.gson:gson:2.4'
    compile 'com.squareup.picasso:picasso:2.5.2'
~~~~


## Building APK
### Building Debug version
~~~
> cd ffh-financial-trainig
> ./gradlew assembleDebug
~~~

### Building Release APK for ALL Languages
This is the *signed* variant.
~~~
> cd ffh-financial-training
> ./gradlew assembleRelease

// Look for it at app/build/outputs/apk/.
~~~


### Building Release for Specific Language (e.g Mossi)
This is the *signed* variant.
~~~
// Building the Mossi language
> cd ffh-financial-training
> ./gradlew assembleMossi

// Look for it at app/build/outputs/apk/app-mossi-release.apk
~~~


### Installing on an Android phone or emulator
~~~
// Connect the phone to your computer (or start the emulator)
> adb install app/build/outputs/apk/app-mossi-release.apk
~~~


### Signing


#### Keystore Notes ####
Keystore is located at `ffh-financial-training/ffh_keystore`. The keystore's password is `freedom`.

This keystore contains a single entry, `freedom`. The password for this entry is also `freedom`.

~~~~
// List the entry
> keytool -list -keystore ffh_keystore -alias freedom
~~~~   


## Deploying
### Deploying on TestFairy
1. Sign into TestFairy (create your own account)
1. Upload the APK